import React from 'react';  
import { render } from 'react-dom';
import { Link, Route, Router, browserHistory } from 'react-router';

import Login from './components/Login.jsx';
import SubLogin from './components/SubLogin.jsx';

const App = React.createClass({  
  render() {
    console.log('re-render')
    return (
      <div className="nav">
        <Link to="">Home </Link>
        <a href="/login">Login </a>
        <Link to="/login">Login </Link>
        <Link to="/login/sub">Login_sub </Link>
        <Link to="/login/sub2">Login_sub2 </Link>

        {this.props.children}
      </div>
    );
  }
});

render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <Route path="login" component={Login}>
        <Route path="sub" component={SubLogin} />
      </Route> 
    </Route> 
  </Router>
), document.getElementById('react'));
