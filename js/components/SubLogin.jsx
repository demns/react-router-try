import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';

export default class SubLogin extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		nextProps.location.key = this.props.location.key;
		// console.log(JSON.stringify(this.props))
		// console.log(JSON.stringify(nextProps))
		// console.log(JSON.stringify(nextProps) !== JSON.stringify(this.props))
		// console.log(JSON.stringify(nextState) !== JSON.stringify(this.state))
		// console.log(shallowCompare(this, nextProps, nextState))
		
		return JSON.stringify(nextProps) !== JSON.stringify(this.props) &&
			   JSON.stringify(nextState) !== JSON.stringify(this.state);

		// return shallowCompare(this, nextProps, nextState);
		// return false;
	}

	render() {
    	console.log('re-render SubLogin')
		return (
			<div className="nav">
				sublogin
			</div>
		);
	}
}
